from face_tracking import random_color


def test_random_color():
    color = random_color()
    assert len(color) == 3
