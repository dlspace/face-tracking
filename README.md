# Face tracking project

## Build and install package

```sh
flit build --no-use-vcs --setup-py
pip install dist/face_tracking-1.0.0-py3-none-any.whl
```

## Install package in develop mode

```sh
flit install 
    --symlink
    --python path/to/python
    --deps=develop
    --extras={dev,test,doc}
```

or

```sh
pip install -e .[dev,test,doc]
```

## Run demo video

Run in console:

```sh
face-tracking --video_in=data/video.mp4 --video_out=data/video_out.mp4 --device=cuda:0
```

## Run camera

```sh
face-tracking --camera_id=0
```
