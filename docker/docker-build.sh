#!/bin/bash

IMG_TAG_IN="nvidia/cuda:12.1.1-cudnn8-runtime-ubuntu22.04"
IMG_TAG_OUT="mlspace/torch:python3.10-cuda12.1.1-cudnn8-runtime-ubuntu22.04"

docker build \
  --no-cache \
  --build-arg IMG_NAME=$IMG_TAG_IN \
  --build-arg "USERNAME=mluser" \
  --build-arg "USERPASSWORD=mlpassword" \
  --tag $IMG_TAG_OUT \
  .
