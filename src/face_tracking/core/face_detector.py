# %%
import abc

import torch
from facenet_pytorch import MTCNN


class IFaceDetector(abc.ABC):
    @abc.abstractmethod
    def detect(self, image):
        pass

    @abc.abstractmethod
    def crop(self, image, batch_bboxes):
        pass


class FaceDetector(IFaceDetector):
    def __init__(self, device, **kwargs):
        self.model = MTCNN(
            margin=14,
            # margin=0,
            keep_all=True,
            factor=0.5,
            # min_face_size=40,
            min_face_size=30,
            device=device,
            **kwargs,
        ).eval()

    def detect(self, image):
        with torch.inference_mode():
            batch_bboxes, batch_probs = self.model.detect(image)
            return batch_bboxes, batch_probs

    def crop(self, image, batch_bboxes):
        return self.model.extract(image, batch_bboxes, save_path=None)

    @staticmethod
    def update_bbox_to_ltwh(bbox):
        x0, x1 = bbox[0], bbox[2]
        y0, y1 = bbox[1], bbox[3]
        w, h = x1 - x0, y1 - y0
        return x0, y0, w, h
