# %%
from pathlib import Path

import cv2


class VideoFrameReader:
    def __init__(self, filename=None, camera_id=0):
        if (filename is not None) and (Path(filename).exists()):
            vmode = filename
        else:
            vmode = camera_id
        self.cap = cv2.VideoCapture(vmode)
        self.width = int(self.cap.get(3))
        self.height = int(self.cap.get(4))
        self.fps = self.cap.get(cv2.CAP_PROP_FPS)

    def __iter__(self):
        return self

    def __next__(self):
        ret, frame = self.cap.read()
        if not ret:
            self.cap.release()
            raise StopIteration
        return frame


class VideoFrameWriter:
    def __init__(self, filename, width, height, fps, fourcc=-1, **kwargs):
        self.width = width
        self.height = height
        self.fps = fps
        self.fourcc = cv2.VideoWriter_fourcc(*"MP4V") if fourcc is None else fourcc
        self.cap = cv2.VideoWriter(
            filename,
            self.fourcc,
            fps,
            (width, height),
            True,
            **kwargs,
        )

    def write(self, frame):
        self.cap.write(frame)
