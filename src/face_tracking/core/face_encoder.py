# %%
import abc

import torch
from facenet_pytorch import InceptionResnetV1


class IFaceEncoder(abc.ABC):
    @abc.abstractmethod
    def encode(self, image):
        pass


class FaceEncoder(IFaceEncoder):
    def __init__(self, device="cpu", pretrained="vggface2", **kwargs):
        self.device = device
        self.model = InceptionResnetV1(
            pretrained=pretrained,
            device=device,
            **kwargs,
        ).eval()

    def encode(self, image):
        image = image.to(self.device)
        with torch.inference_mode():
            embeddings = self.model(image)
            return embeddings
