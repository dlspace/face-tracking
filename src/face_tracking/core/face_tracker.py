# %%
import abc

from deep_sort_realtime.deepsort_tracker import DeepSort


class IFaceTracker(abc.ABC):
    @abc.abstractmethod
    def update_tracks(self, detections, embeddings, frame):
        pass


class FaceTracker(IFaceTracker):
    def __init__(self, device="cpu"):
        self.model = DeepSort(
            max_age=30,
            max_cosine_distance=0.4,
            embedder_gpu=True,
        )

    def update_tracks(self, detections, embeddings, frame, **kwargs):
        return self.model.update_tracks(
            detections,
            embeds=embeddings,
            frame=frame,
            **kwargs,
        )
