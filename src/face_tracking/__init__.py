"""Face Tracking Docstring"""

__version__ = "1.0.0"


from .core.face_detector import FaceDetector
from .core.face_encoder import FaceEncoder
from .core.face_tracker import FaceTracker
from .core.video_capture import VideoFrameReader, VideoFrameWriter
from .utils.random_color import random_color

__all__ = [
    "FaceDetector",
    "FaceEncoder",
    "FaceTracker",
    "VideoFrameReader",
    "VideoFrameWriter",
    "random_color",
]
