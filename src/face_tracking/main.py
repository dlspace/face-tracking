# %%
import os
import random

import click
import cv2
import numpy as np
import torch
from PIL import Image

from face_tracking import (
    FaceDetector,
    FaceEncoder,
    FaceTracker,
    VideoFrameReader,
    VideoFrameWriter,
    random_color,
)


# %%
def seed_all(num=42):
    torch.manual_seed(num)
    torch.cuda.manual_seed_all(num)
    torch.backends.cudnn.deterministic = True
    torch.backends.cudnn.benchmark = False
    np.random.seed(num)
    random.seed(num)
    os.environ["PYTHONHASHSEED"] = str(num)


def select_device(device_str):
    if "cuda" in device_str and torch.cuda.is_available():
        device = torch.device(device_str)
    else:
        device = torch.device("cpu")
    print(f"Running on device: {device}")
    return device


@click.command()
@click.option("--device", type=click.STRING, default="cpu")
@click.option("--camera_id", type=click.INT, default=-1)
@click.option("--video_in", type=click.Path(), default=None)
@click.option("--video_out", type=click.Path(), default="data/video_out.mp4")
def main(device, camera_id, video_in, video_out):
    seed_all(42)
    device = select_device(device)

    faces_detector = FaceDetector(device)
    faces_encoder = FaceEncoder(device)
    faces_tracker = FaceTracker(device)

    colors = {}
    tracks = {}

    if video_in is not None:
        video_in = VideoFrameReader(filename=video_in)
    else:
        video_in = VideoFrameReader(camera_id=camera_id)

    video_out = VideoFrameWriter(
        filename=video_out,
        width=video_in.width,
        height=video_in.height,
        fps=video_in.fps,
    )
    for frame in video_in:
        image = Image.fromarray(frame)
        batch_bboxes, batch_probs = faces_detector.detect(image)
        faces_cropped = faces_detector.crop(image, batch_bboxes)
        faces_embeddings = faces_encoder.encode(faces_cropped).tolist()

        if batch_bboxes is None:
            continue

        detections = []
        embeddings = []
        for i, bbox in enumerate(batch_bboxes):
            prob = batch_probs[i]
            if prob < 0.90:
                continue
            bbox_ltwh = faces_detector.update_bbox_to_ltwh(bbox)
            detections.append((bbox_ltwh, prob, "face"))
            embeddings.append(faces_embeddings[i])

        tracks = faces_tracker.update_tracks(
            detections,
            embeddings=embeddings,
            frame=frame,
        )

        for track in tracks:
            if not track.is_confirmed():
                continue
            track_id = track.track_id
            colors[track_id] = colors.get(track_id, random_color())
            ltrb = track.to_ltrb()
            bbox = ltrb

            cv2.rectangle(
                frame,
                (int(bbox[0]), int(bbox[1])),
                (int(bbox[2]), int(bbox[3])),
                colors[track_id],
                2,
            )
            cv2.putText(
                frame,
                f"ID: {track_id}",
                (int(bbox[0]), int(bbox[1] - 10)),
                cv2.FONT_HERSHEY_SIMPLEX,
                0.5,
                colors[track_id],
                2,
            )

        cv2.imshow("IMG", frame)
        video_out.write(frame)

        k = cv2.waitKey(1)
        if k % 256 == 27:  # ESC
            print("Esc pressed, closing...")
            break

    video_out.cap.release()
    cv2.destroyAllWindows()


if __name__ == "__main__":
    main()
