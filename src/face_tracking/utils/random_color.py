import random


def random_color():
    return random.sample(range(0, 255), 3)
